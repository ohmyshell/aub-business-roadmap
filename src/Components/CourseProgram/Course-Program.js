import React from 'react';
import Courses from './Courses';

class CourseProgram extends React.Component {
  constructor() {
    super();
    this.state = {
      core: [],
      concentration: [],
      nonbusiness: [],
    };
  }

  async componentDidMount() {
    if (localStorage.getItem('course-program')) {
      let dataLocal = JSON.parse(localStorage.getItem('course-program'));
      if (dataLocal) {
        this.setState({
          core: dataLocal.core,
          concentration: dataLocal.concentration,
          nonbusiness: dataLocal.nonbusiness,
        });
      }
    } else {
      try {
        let response = await fetch(
          'https://mycorsanywhere.herokuapp.com/https://jsonbase.com/arp-aub/course-program'
        );
        let data = await response.json();
        this.setState({
          core: data.core,
          concentration: data.concentration,
          nonbusiness: data.nonbusiness,
        });
        localStorage.setItem('course-program', JSON.stringify(this.state));
        localStorage.setItem('is-online', 'true');
      } catch (error) {
        console.error(error, 'Server request failed. Server offline.');
        localStorage.setItem('is-online', 'false');
      }
    }
    if (!this.state.core) {
    } else {
    }
    if (!this.state.core) localStorage.setItem('is-online', 'false');
  }

  calculateTotal() {
    let total = 0;
    this.state.core.forEach((element) => {
      total += element.credits;
    });
    this.state.concentration.forEach((element) => {
      total += element.credits;
    });
    this.state.nonbusiness.forEach((element) => {
      total += element.credits;
    });
    return total;
  }

  calculateTotalGrades() {
    let total = 0;
    let count = 0;
    this.state.core.forEach((element) => {
      total += Number(element.grade) * element.credits;
      if (Number(element.grade) !== 0) {
        count += element.credits;
      }
    });
    this.state.concentration.forEach((element) => {
      total += Number(element.grade) * element.credits;
      if (Number(element.grade) !== 0) {
        count += element.credits;
      }
    });
    this.state.nonbusiness.forEach((element) => {
      total += Number(element.grade) * element.credits;
      if (Number(element.grade) !== 0) {
        count += element.credits;
      }
    });
    return total / count;
  }

  calculateTaken() {
    let takenTotal = 0;
    this.state.core.forEach((element) => {
      if (element.taken) takenTotal += element.credits;
    });
    this.state.concentration.forEach((element) => {
      if (element.taken) takenTotal += element.credits;
    });
    this.state.nonbusiness.forEach((element) => {
      if (element.taken) takenTotal += element.credits;
    });
    return takenTotal;
  }

  async save() {
    if(localStorage.getItem('course-program'))
      localStorage.removeItem('course-program');
    let text = JSON.stringify(this.state);
    let response = await fetch(
      'https://mycorsanywhere.herokuapp.com/https://jsonbase.com/arp-aub/course-program',
      {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        headers: {
          'Content-Type': 'application/json',
          // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: text, // body data type must match "Content-Type" header
      }
    );
    let status = await response.status;
    if (status === 200) return true;
  }

  changeGrade(i, type, value) {
    switch (type) {
      case 'core':
        let core = this.state.core;
        core.splice(i, 1, {
          id: this.state.core[i].id,
          name: this.state.core[i].name,
          credits: this.state.core[i].credits,
          taken: this.state.core[i].taken,
          grade: value,
          info: this.state.core[i].info,
        });
        this.setState({ core: core });
        break;
      case 'concentration':
        let concentration = this.state.concentration;
        concentration.splice(i, 1, {
          id: this.state.concentration[i].id,
          name: this.state.concentration[i].name,
          credits: this.state.concentration[i].credits,
          taken: this.state.concentration[i].taken,
          grade: value,
          info: this.state.concentration[i].info,
        });
        this.setState({ concentration: concentration });
        break;
      case 'nonbusiness':
        let nonbusiness = this.state.nonbusiness;
        nonbusiness.splice(i, 1, {
          id: this.state.nonbusiness[i].id,
          name: this.state.nonbusiness[i].name,
          credits: this.state.nonbusiness[i].credits,
          taken: this.state.nonbusiness[i].taken,
          grade: value,
          info: this.state.concentration[i].info,
        });
        this.setState({ nonbusiness: nonbusiness });
        break;
      default:
        break;
    }
  }

  changeTaken(i, type) {
    switch (type) {
      case 'core':
        let core = this.state.core;
        core.splice(i, 1, {
          id: this.state.core[i].id,
          name: this.state.core[i].name,
          credits: this.state.core[i].credits,
          taken: !this.state.core[i].taken,
          grade: this.state.core[i].grade,
          info: this.state.core[i].info,
        });
        this.setState({ core: core });
        break;
      case 'concentration':
        let concentration = this.state.concentration;
        concentration.splice(i, 1, {
          id: this.state.concentration[i].id,
          name: this.state.concentration[i].name,
          credits: this.state.concentration[i].credits,
          taken: !this.state.concentration[i].taken,
          grade: this.state.concentration[i].grade,
          info: this.state.concentration[i].info,
        });
        this.setState({ concentration: concentration });
        break;
      case 'nonbusiness':
        let nonbusiness = this.state.nonbusiness;
        nonbusiness.splice(i, 1, {
          id: this.state.nonbusiness[i].id,
          name: this.state.nonbusiness[i].name,
          credits: this.state.nonbusiness[i].credits,
          taken: !this.state.nonbusiness[i].taken,
          grade: this.state.nonbusiness[i].grade,
          info: this.state.nonbusiness[i].info,
        });
        this.setState({ nonbusiness: nonbusiness });
        break;
      default:
        break;
    }
  }

  render() {
    return Object.keys(this.state.core).length === 0 ? (
      <h3>Loading</h3>
    ) : (
      <>
        <h4> Core</h4>
        <Courses
          data={this.state.core}
          type="core"
          functions={{
            changeTaken: this.changeTaken.bind(this),
            changeGrade: this.changeGrade.bind(this),
          }}
        />
        <h4>Concentration</h4>
        <Courses
          data={this.state.concentration}
          type="concentration"
          functions={{
            changeTaken: this.changeTaken.bind(this),
            changeGrade: this.changeGrade.bind(this),
          }}
        />
        <h4>Non-Business</h4>
        <Courses
          data={this.state.nonbusiness}
          type="nonbusiness"
          functions={{
            changeTaken: this.changeTaken.bind(this),
            changeGrade: this.changeGrade.bind(this),
          }}
        />
        <label>
          Total Credits {this.calculateTotal()} | Taken Credits{' '}
          {this.calculateTaken()} | Grades Average{' '}
          {this.calculateTotalGrades().toFixed(2)}
        </label>
        <button
          className="button w-100 add"
          onClick={this.save.bind(this)}
          disabled={localStorage.getItem('is-online') === 'false'}
        >
          Save
        </button>
      </>
    );
  }
}

export default CourseProgram;
