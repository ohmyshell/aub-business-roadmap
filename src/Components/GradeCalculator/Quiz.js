import React from 'react';
class Quiz extends React.Component {

	render() {
		return (
			<div className="flex">
				<label>Grade</label><input type="number" value={this.props.grade} min={0} onChange={this.props.functions.grade} pattern="[0-9]*" />
				<label>Percent</label><input type="number" value={this.props.percent} min={0} max={100} onChange={this.props.functions.percent} pattern="[0-9]*" />
				<button className="button delete" onClick={this.props.functions.delete}>Delete</button>
				<hr />
			</div>
		)
	}
}

export default Quiz